const express = require('express');
const { copyFileSync } = require('fs');
const { get } = require('http');
const app = express();

app.use(express.static('abc'));

const mysql = require('mysql2');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'cdac',
    database: 'ameypatekar_20_jh',
	port:3306
});

connection.query('select * from book',[],(err,rows)=>{
    console.log(rows);
});


app.get('/getdetails',(req,res)=>{
        let input=req.query.x;
        let output={status:false,bname:"",bprice:"",message:"bookid not found"};

        connection.query('select * from book where bookid=?',[input],(err,rows)=>{
            if(err){
                console.log("Error"+err);
            }
            else{
                if(rows.length>0)
                {
                    output.status=true;
                    output.message="bookid is found";
                    output.bname=rows[0].bookname;
                    output.bprice=rows[0].price;
                    console.log(rows.length);
                }
            }
            res.send(output);
        });
});

app.get('/update',(req,res)=>{
    let boodid=req.query.bid;
    let bookname=req.query.bname;
    let bookprice=req.query.bprice;
    let output={status:false,message:"not updated "};

    connection.query('update book set price=?,bookname=? where bookid=?',[bookprice,bookname,boodid],(err,rows)=>{
        if(err){
            console.log(err);
        }
        else{
            if(rows.affectedRows>0)
            {
                output.status=true;
                output.message="updated successfully";

                console.log(rows.affectedRows);

            }
        }
        res.send(output);
    });
});


app.get('/showall',(req,res)=>{
    connection.query('select * from book',[],(err,rows)=>{
        if(err){
            console.log(err);
        }
        else{
            if(rows.length>0)
            {
                console.log(rows.length);

            }
        }
        res.send(rows);
    });
});


app.listen(8081, function () {
    console.log("server listening at port 8081...");
});


//Code is working 